# gitlab-registry-starter-npm

Minimal example of a NPM/YARN Registry project with Gitlab.

```
export NPM_TOKEN=xxxxxxxxxxx
npm config set '//gitlab.com/api/v4/projects/20248482/packages/npm/:_authToken' "${NPM_TOKEN}"
npm config set '//gitlab.com/api/v4/packages/npm/:_authToken' "${NPM_TOKEN}"
```

## Resources

* https://docs.gitlab.com/ee/user/packages/npm_registry/index.html
* https://docs.gitlab.com/ee/user/packages/